FROM openjdk:11
COPY target /usr/src/testapp
WORKDIR /usr/src/testapp
RUN chmod 777 sonarscanner-maven-basic-1.0-SNAPSHOT.jar
CMD ["java", "-jar", "sonarscanner-maven-basic-1.0-SNAPSHOT.jar"]